#ifndef TRACK_H
#define TRACK_H

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

#include "track_piece.h"

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

// Struct for representing a lane
typedef struct {
	float lane_distance;
	std::vector<float> lane_length;
	std::vector<float> lane_radius;
	std::vector<float> max_speed;
} lane;

class track
{
public:
    track();
    int add_piece(float length, float radius, float angle, int lane_switch);
    int get_num_of_pieces();
    track_piece& get_track_piece(unsigned int num);
    float get_track_length(){ return track_length; }
    void add_lane(unsigned int index, float dist_from_center);
    float get_lane_distance(unsigned int index);
    void compute_lane_length();
    void compute_max_piece_speeds(); // Actually computes max lane speed at each piece
    float get_lane_length(unsigned int piece, unsigned int lane);
    float get_lane_max_vel(unsigned int piece, unsigned int lane);
    unsigned int get_next_bend(unsigned int piece_index);
    void add_max_angular_vel(float max_ang_vel) { max_angular_velocity += max_ang_vel; }
    float get_lane_radius(unsigned int piece_index, unsigned int lane) { return lanes[lane].lane_radius[piece_index]; }
    unsigned int get_num_lanes() { return lanes.size(); }
	int findLongestStraightLine();
private:
    std::vector<track_piece> trackpieces;
    float track_length;
    std::map<unsigned int,lane> lanes;
    float max_angular_velocity; // Max angular velocity, depends on the track friction
    int longest_straight_line; // Used for turbo! :)

};

#endif // TRACK_H
