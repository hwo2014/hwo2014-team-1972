#ifndef HWO_CONNECTION_TEXT_H
#define HWO_CONNECTION_TEXT_H

#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <jsoncons/json.hpp>
#include "connection.h"

using boost::asio::ip::tcp;

class hwo_connection_text : public hwo_connection
{
public:
  hwo_connection_text();
  hwo_connection_text(const std::string& host, const std::string& port);
  virtual ~hwo_connection_text();
  virtual jsoncons::json receive_response(boost::system::error_code& error);
  virtual void send_requests(const std::vector<jsoncons::json>& msgs);

private:

  std::ifstream JSONFile;	// File to write server json messages
};

#endif
