#include "track_piece.h"

#define DEV_MODE 1

track_piece::track_piece()
{
}


track_piece::track_piece(float length_, float radius_, float angle_, int lane_switch_, float dist):
    length(length_),
    radius(radius_),
    angle(angle_),
    lane_switch(lane_switch_),
    dist_from_start(dist)
{
    if(DEV_MODE)
    {
        std::cout << "New piece: ";
        std::cout << "length= " << length;
        std::cout << " radius= " << radius;
        std::cout << " angle= " << angle;
        std::cout << " lane switch= " << lane_switch;
        std::cout << " distance from start= " << dist_from_start;
        std::cout << std::endl;
    }
}

bool track_piece::isBend()
{
	if(radius > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
	return false;
}

float track_piece::getLength()
{
	return length;
}

float track_piece::getRadius()
{
	return radius;
}

float track_piece::getAngle()
{
	return angle;
}

