/*
 * car.cpp
 *
 *  Created on: Apr 20, 2014
 *      Author: jolaakso
 */
#include "car.h"

car::car() :
name(""), color(""), length(0.0), width(0.0), lane(0), piece_index(0), speed(0.0), acc(0.0), angle(0.0), angle_speed(0.0), angle_acc(0.0), guide_flag_pos(0.0), in_piece_pos(0.0), estTickCount(50), brake_until_piece(-1),
lane_change(0), turboDurationMilliseconds(0.0), turboDurationTicks(0), turboFactor(0), brake_until_pos(0.0), brake_until_piece_overflow(false)
{

}

car::car(std::string name_, std::string color_, float length_, float width_, float guide_flag_pos_) :
		name(""), color(""), length(0.0), width(0.0), lane(0), piece_index(0), speed(0.0), acc(0.0), angle(0.0), angle_speed(0.0), angle_acc(0.0),
        guide_flag_pos(0.0), in_piece_pos(0.0), estTickCount(50), brake_until_piece(-1), lane_change(0), turboDurationMilliseconds(0.0), turboDurationTicks(0), turboFactor(0), brake_until_pos(0.0), brake_until_piece_overflow(false)
{
	name = name_;
	color = color_;
	length = length_;
	width = width_;
	guide_flag_pos = guide_flag_pos_;
}

float car::computeThrottle()
{
	// Setup current info
	float cur_speed = speed;
	float cur_pos = in_piece_pos;
	unsigned int cur_piece_index = piece_index;
	unsigned int cur_lane = lane;


	// Compute the throttle response
	// If already at the max speed or above
	if(brake_until_piece > -1)
	{
		if(cur_piece_index < brake_until_piece || (brake_until_piece_overflow == true && cur_piece_index > brake_until_piece))
		{
			if(racetrack->get_lane_max_vel(brake_until_piece, lane) > cur_speed)
			{
				if(racetrack->get_lane_max_vel(brake_until_piece, lane)/10.0 > 1.0)
				{
					return 1.0;
				}
				else
				{
					return racetrack->get_lane_max_vel(brake_until_piece, lane)/10.0;
				}
			}
			else
			{
				return 0.0;
			}
		}
		else
		{
			/*if(brake_until_pos > cur_pos && brake_until_piece == cur_piece_index)
			{
				return 0.0;
			}
			else
			{*/
				brake_until_piece = -1;
				brake_until_piece_overflow = false;
				if(racetrack->get_lane_max_vel(cur_piece_index, lane)/10.0 > 1.0)
				{
					return 1.0;
				}
				else
				{
					return racetrack->get_lane_max_vel(cur_piece_index, lane)/10.0;
				}
			//}
		}
	}
	else
	{
		// Estimate the position and speed n ticks ahead if zero throttle
		for(unsigned int i = 0;i<estTickCount;i++)
		{
			acc = estimateAcceleration(0.0, cur_speed);
			cur_speed += acc;
			float piece_length = racetrack->get_track_piece(cur_piece_index).getLength();
			if(cur_pos + cur_speed > piece_length)
			{

				cur_pos = cur_speed - (piece_length-cur_pos);
				cur_piece_index = racetrack->get_num_of_pieces()-1 > cur_piece_index+1 ? cur_piece_index+1 : 0;
				if(lane_change != cur_lane && racetrack->get_track_piece(cur_piece_index).getLaneSwitch() == 1)
				{
					cur_lane = lane_change;
				}
				if(racetrack->get_lane_max_vel(cur_piece_index, cur_lane) < cur_speed )
				{
					std::cout <<"Predicted speed " << cur_speed << " Max speed " << racetrack->get_lane_max_vel(cur_piece_index, cur_lane) << " at piece " << cur_piece_index << " " << cur_pos << " " << cur_lane << std::endl;
					brake_until_piece = cur_piece_index;
					brake_until_pos = cur_pos;
					if(brake_until_piece < piece_index)
					{
						brake_until_piece_overflow = true;
					}
					else
					{
						brake_until_piece_overflow = false;
					}
					return 0.0;
				}

			}
			else
			{
				cur_pos += cur_speed;
				if(racetrack->get_lane_max_vel(cur_piece_index, cur_lane) < cur_speed )
				{
					std::cout <<"Predicted speed " << cur_speed << " Max speed " << racetrack->get_lane_max_vel(cur_piece_index, cur_lane) << " at piece " << cur_piece_index << " " << cur_pos << " " << cur_lane << std::endl;
					brake_until_piece = cur_piece_index;
					brake_until_pos = cur_pos;
					return racetrack->get_lane_max_vel(cur_piece_index, cur_lane)/10.0;
				}
			}
		}
		return 1.0;
	}

	return 0.5;

}
float car::estimateAcceleration(float throttle, float cur_speed)
{
	float acc = 0.0;
	if(throttle*10 < cur_speed)
	{
		// Simple estimation for now
		acc = ((throttle*10)-cur_speed)/80.0;
	}
	else
	{
		acc = ((throttle*10)-cur_speed)/40.0;
	}
	return acc;
}
void car::setTrack(track* racetrack_)
{
	racetrack = racetrack_;
}

void car::setPosition(unsigned int idx, float pos)
{
	// If the piece_index changes, we need to compute the length traveled in previous piece
	if(idx != piece_index)
	{
		float lane_piece_length = racetrack->get_lane_length(piece_index,lane);
		acc = ((lane_piece_length-in_piece_pos)+pos)-speed;
		speed = ((lane_piece_length-in_piece_pos)+pos);
		std::cout<< lane_piece_length << " " << in_piece_pos << " " << pos << std::endl;
		in_piece_pos = pos;
		piece_index = idx;

	}
	else
	{
		acc = (pos-in_piece_pos)-speed;
		speed = (pos-in_piece_pos);
		in_piece_pos = pos;
	}
	track_piece& tp = racetrack->get_track_piece(piece_index);
	if(!tp.isBend())
	{
		std::cout<<"Updating car "<< color <<": Current speed " << speed << " Acc " << acc <<" Piece index " << piece_index << std::endl;
	}
	else
	{
		std::cout	<<"Updating car "<< color <<": Current angular speed " << speed/(racetrack->get_lane_radius(piece_index, lane))
					<< " Angle " << angle << " angle speed " << angle_speed << " Piece index " << piece_index << std::endl;
	}
}

int car::getLaneChange()
{
	// Check if there are actually more than one car on track
	if(cars->size() > 1)
	{
			for(auto iter = cars->begin();iter != cars->end();iter++)
			{
				if(iter->second.getColor() != color)
				{
					// Find cars that are close and on the same lane
					if((iter->second.getPositionIndex() <= this->piece_index+2 && iter->second.getPositionIndex() >= this->piece_index) && iter->second.getLane() == lane)
					{
						// Check if the car has lower speed than we can drive
						if((racetrack->get_track_piece(iter->second.getPiecePosition()).isBend() && racetrack->get_track_piece(iter->second.getPiecePosition()).getMaxSpeed() > iter->second.getSpeed()) ||
								(!racetrack->get_track_piece(iter->second.getPiecePosition()).isBend() && iter->second.getSpeed() <= speed+0.1))
						{
							// Find out where to change
							if(racetrack->get_num_lanes()-1 == lane)
							{
								this->lane_change = lane-1;

								// Change to left
								return -1;
							}
							else
							{
								this->lane_change = lane+1;
								// Change to right
								return 1;
							}
						}
					}
				}
			}
	}
	// Return -1 for lane change to left, 1 for right and 0 for no change
	return 0;
}

void car::setAngle(float angle_)
{
	angle_acc = (angle_-angle)-angle_speed;
	angle_speed = (angle_-angle);
	angle = angle_;
}
void car::setLane(unsigned int lane_)
{

	lane = lane_;
}

void car::setTurbo(float durationMilliseconds_, unsigned int durationTicks_, float factor_)
{
    turboDurationMilliseconds = durationMilliseconds_;
    turboDurationTicks = durationTicks_;
    turboFactor = factor_;

    std::cout << "TURBO!! duration " << turboDurationMilliseconds <<  " (ms) " <<
                 " duration " << turboDurationTicks <<  " (ticks) " <<
                 " factor " << turboFactor << std::endl;
}

unsigned int car::getPositionIndex()
{
	return piece_index;
}
float car::getPiecePosition()
{
	return in_piece_pos;
}
float car::getSpeed()
{
	return speed;
}
float car::getAngle()
{
	return angle;
}
float car::getAngleSpeed()
{
	return angle_speed;
}
float car::getAngleAcc()
{
	return angle_acc;
}
float car::getTurboDurationMilliseconds()
{
    return turboDurationMilliseconds;
}
unsigned int car::getTurboDurationTicks()
{
    return turboDurationTicks;
}
float car::getTurboFactor()
{
    return turboFactor;
}
