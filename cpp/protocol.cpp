#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_race(const std::string& name, const std::string& key, const std::string& track)
  {
	jsoncons::json data;
	jsoncons::json botid;
	botid["name"] = name;
	botid["key"] = key;
	data["botId"] = botid;
	data["trackName"] = track;
	data["password"] = std::string("1234");
	data["carCount"] = 1;
	return make_request("createRace", data);
  }

  jsoncons::json make_race_join(const std::string& name, const std::string& key, const std::string& track)
  {
	jsoncons::json data;
	jsoncons::json botid;
	botid["name"] = name;
	botid["key"] = key;
	data["botId"] = botid;
	data["trackName"] = track;
	data["password"] = std::string("1234");
	data["carCount"] = 1;
	return make_request("joinRace", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

  jsoncons::json make_change_lane(std::string direction)
  {
      return make_request("switchLane", direction);
  }

  jsoncons::json make_turbo(std::string message)
  {
      return make_request("turbo", message);
  }

}  // namespace hwo_protocol
