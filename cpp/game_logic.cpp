#include "game_logic.h"
#include "protocol.h"

#include "track.h"
#include "track_piece.h"

#define PRINT_CARDATA 1

using namespace hwo_protocol;

game_logic::game_logic()
  : my_car_crashed(false), initialized(false), action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car},
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "turboAvailable", &game_logic::on_turbo_available},
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "spawn", &game_logic::on_spawn }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json &data)
{
    std::cout << "Your car!1";

    my_car_color = data.get("color").as<std::string>();
    my_car_name = data.get("name").as<std::string>();

    std::cout << "name= " << my_car_name << " color= " << my_car_color  << std::endl;

    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json &data)
{
	// If already initialized (qualifiers -> race) don't initialize
	if(!this->initialized)
	{
		std::cout << "game init!1" << std::endl;
		this->initialized = true;

		const auto& race = data["race"];
		const auto& track = race["track"];
		const auto& lanes = track["lanes"];
		const auto& pieces = track["pieces"];
		const auto& cars = race["cars"];

		std::cout << "Track id: " << track["id"].as<std::string>() << std::endl;

		std::cout << "Number of pieces: " << pieces.size() << std::endl;

		for(size_t i=0; i < pieces.size(); i++)
		{
			/* Parse track pieces information and add new pieces to racetrack */
			const auto& piece = pieces[i];

			/* Convert data to a proper format (float did not work?!) */
			double len = piece.get("length", 0).as<double>();
			double rad = piece.get("radius",0).as<double>();
			double ang = piece.get("angle", 0).as<double>();
			int   swi = piece.get("switch", 0).as<int>();

			/* Add new piece to racetrack */
			racetrack.add_piece((float) len, (float) rad, (float) ang, swi);
		}

		std::cout << "number of pieces: " << racetrack.get_num_of_pieces();
		std::cout << " track length: " << racetrack.get_track_length() << std::endl;

		// Input lanes
		for(size_t i=0; i < lanes.size(); i++)
		{
			const auto& lane = lanes[i];
			racetrack.add_lane(lane.get("index").as<unsigned int>(),(float)lane.get("distanceFromCenter").as<double>());
			std::cout << "Added lane " << lane.get("index").as<unsigned int>() << " with distance " << lane.get("distanceFromCenter").as<double>() << std::endl;
		}

		// Compute lane lengths
		racetrack.compute_lane_length();
		// Compute max lane speeds per piece based on maximum angular velocity (default 0.06 rad/tick)
		racetrack.compute_max_piece_speeds();

		// Update cars
		for(size_t i=0; i < cars.size(); i++)
		{
			const auto& car_id = cars[i]["id"];
			const auto& car_dimensions = cars[i]["dimensions"];

			std::string color = car_id.get("color").as<std::string>();
			car new_car(car_id.get("name").as<std::string>(), color,
					(float)car_dimensions.get("length").as<double>(), (float)car_dimensions.get("length").as<double>(),
					(float)car_dimensions.get("guideFlagPosition").as<double>());

			car_map[color] = new_car;
			car_map[color].setTrack(&racetrack);
			std::cout << "Added car " << color << std::endl;

		}

        // Find the longest straight part of the racetrack
        racetrack.findLongestStraightLine();

		// Give the player car pointer to other cars to make passing possible
		car_map[my_car_color].setCars(&car_map);
	}

    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  throttle = 0.0;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  // Update car positions&angles
	//"data":[{"id":{"name":"Flat tires","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}]


  // Decrease turbo duration ticks to not trigger the turbo randomly
	//car_map[my_car_color].addToTurboDurationTicks(-1);

  for(size_t i=0; i < data.size(); i++)
  {
	  const auto& id = data[i].get("id");
	  const auto& pos = data[i].get("piecePosition");
	  const auto& lane =pos.get("lane");
	  float angle = (float)data[i].get("angle").as<double>();
	  std::string color = id.get("color").as<std::string>();

	  // Set lane first, affects the position/speed calculation
	  car_map[color].setLane(lane.get("startLaneIndex").as<unsigned int>());
	  car_map[color].setAngle(angle);
	  car_map[color].setPosition(pos.get("pieceIndex").as<unsigned int>(),(float)pos.get("inPieceDistance").as<double>());
  }

  /* TODO: write an intelligent acceleration control */
  //unsigned int nextBend = racetrack.get_next_bend(car_map[color].getPiecePosition());

  if(PRINT_CARDATA)
  {
      std::cout << "CARDATA: " << car_map[my_car_color].getPiecePosition() << " ";
      std::cout << car_map[my_car_color].getPositionIndex() << " ";
      std::cout << car_map[my_car_color].getSpeed() << " ";
      std::cout << car_map[my_car_color].getAngle() << " ";
      std::cout << car_map[my_car_color].getAngleSpeed() << " ";
      std::cout << car_map[my_car_color].getAngleAcc() << " ";
      std::cout << racetrack.get_track_piece(car_map[my_car_color].getPositionIndex()).getRadius() << " ";
      std::cout << racetrack.get_track_piece(car_map[my_car_color].getPositionIndex()).getAngle();
      std::cout << std::endl;
  }

  // Either send lane change, turbo or throttle message here, only one can be done at a time
  int lane_change = car_map[my_car_color].getLaneChange();

  if(car_map[my_car_color].getPositionIndex()==racetrack.findLongestStraightLine() && car_map[my_car_color].getTurboDurationTicks() > 0 )
  {
	  std::cout << "TURBOOOO!" << std::endl;
	  /* Reset turbo */
	  car_map[my_car_color].setTurbo(0.0,0,0.0);
	  return { make_turbo("KABOOM!!") };
  }
  else if( lane_change != 0)
  {
	std::cout << "Changing lane :" << lane_change << std::endl;
	if(lane_change == -1)
	{
	  return { make_change_lane("Left") };
	}
	else
	{
	  return { make_change_lane("Right") };
	}
  }
  else
  {
  return { make_throttle(car_map[my_car_color].computeThrottle()) };
  }
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  std::string crash_color = data.get("color").as<std::string>();
  if(crash_color == my_car_color)
  {
	  my_car_crashed = true;
	  // Update max ang vel
	  racetrack.add_max_angular_vel(-0.02);
	  racetrack.compute_max_piece_speeds();
  }
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json &data)
{
    float durationMilliseconds = (float) data.get("turboDurationMilliseconds").as<double>();
    unsigned int durationTicks = data.get("turboDurationTicks").as<int>();
    float factor = (float) data.get("turboFactor").as<double>();

    car_map[my_car_color].setTurbo(durationMilliseconds, durationTicks, factor);

    return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
  std::cout << "Someone spawned" << std::endl;
  std::string crash_color = data.get("color").as<std::string>();
  if(crash_color == my_car_color)
  {
	  my_car_crashed = false;

  }
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
