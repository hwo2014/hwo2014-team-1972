#include <cmath>

#include "track.h"

#define PI 3.14159265359

track::track(): track_length(0.0f), max_angular_velocity(0.45),
	longest_straight_line(-1)
{
}

int track::add_piece(float length, float radius, float angle, int lane_switch)
{
    length = length + fabs((radius*angle*PI)/180);
    trackpieces.push_back(track_piece(length, radius, angle, lane_switch, track_length));
    track_length += length;
    return 0;
}
track_piece& track::get_track_piece(unsigned int num)
{
	if(num > trackpieces.size()-1) {
		return trackpieces[trackpieces.size()-num];
	}
	else
	{
		return trackpieces[num];
	}
}

int track::get_num_of_pieces()
{
    return trackpieces.size();
}

void track::add_lane(unsigned int index, float dist_from_center)
{
	lane new_lane;

	// Fill the lane structure
	new_lane.lane_distance = dist_from_center;

	lanes[index] = new_lane;
}

float track::get_lane_distance(unsigned int index)
{
	return lanes[index].lane_distance;
}

void track::compute_lane_length()
{

	// Compute accumulated length for each lane, ultimately resulting in total lane lengths
	for(unsigned int i=0;i<trackpieces.size();i++)
	{
		for(auto iter = lanes.begin();iter != lanes.end();iter++)
		{
			float prev_length = 0;
			if(i != 0)
			{
				prev_length = iter->second.lane_length[i-1];
			}
			// Handle bends differently as the track length varies between lanes
			if(trackpieces[i].isBend())
			{
				float bend_radius = trackpieces[i].getRadius();
				int sign = sgn<float>(trackpieces[i].getAngle())*sgn<float>(iter->second.lane_distance);
				// Compute coefficient for the radius, either above 1 or below depending on the sign
				float coeff = (bend_radius-((float)sign*fabs(iter->second.lane_distance)))/bend_radius;
				iter->second.lane_length.push_back(trackpieces[i].getLength()*coeff+prev_length);
				iter->second.lane_radius.push_back(coeff*bend_radius);
			}
			else
			{
				iter->second.lane_length.push_back(trackpieces[i].getLength()+prev_length);
				iter->second.lane_radius.push_back(0);
			}

			std::cout<<"Piece " << i << "Lane radius " << iter->second.lane_radius[i] << std::endl;
		}
	}

	for(auto iter = lanes.begin();iter != lanes.end();iter++)
	{
		std::cout<<"Lane " << iter->first << " length is " << iter->second.lane_length[iter->second.lane_length.size()-1] << std::endl;
	}

}

void track::compute_max_piece_speeds()
{
	// empty max speed
	for(unsigned int i=0;i<lanes.size();i++)
	{
		lanes[i].max_speed.clear();
	}
	for(unsigned int i=0;i<trackpieces.size();i++)
	{
		for(auto iter = lanes.begin();iter != lanes.end();iter++)
		{
			if(!trackpieces[i].isBend())
			{
				iter->second.max_speed.push_back(10000.0); // Just a big number here for straight lanes
			}
			else
			{
				// Compute the max speed from the known maximum angle velocity, angle and radius
				float max_ang_speed = sqrt(iter->second.lane_radius[i]*this->max_angular_velocity);
				iter->second.max_speed.push_back(max_ang_speed);

				std::cout<<"Piece "<< i << " Lane vel:" << iter->second.lane_radius[i]*this->max_angular_velocity << "Lane radius:" << iter->second.lane_radius[i] << std::endl;
			}
		}
	}
}
float track::get_lane_max_vel(unsigned int piece, unsigned int lane)
{
	return lanes[lane].max_speed[piece];
}
float track::get_lane_length(unsigned int piece, unsigned int lane)
{
	if(piece != 0) {
		return lanes[lane].lane_length[piece]-lanes[lane].lane_length[piece-1];
	}
	return lanes[lane].lane_length[0];
}



/*
 * This function searches for the next bend.. could be used for accelerating the car
 */
unsigned int track::get_next_bend(unsigned int piece_index)
{
    for(unsigned int i = piece_index; i < trackpieces.size(); i++)
    {
        if(trackpieces.at(i).isBend())
        {
            return i;
        }
    }
    /* If the curve is on the next lap.. we must continue the search */
    for(unsigned int i = 0; i < trackpieces.size(); i++)
    {
        if(trackpieces.at(i).isBend())
        {
            return i;
        }
    }
}

/* The most naive method to find out the longest straight line */
int track::findLongestStraightLine()
{
    /* If the longest straight line is not set */
    if(longest_straight_line<0)
    {
        std::vector<int> straight_pieces;
        int max_length = 0;

        for(unsigned int i = 0; i < trackpieces.size(); i++ )
        {
            unsigned int c = get_next_bend(i);
            /* curve is on the next lap */
            if(c < i)
            {
                c = c + get_num_of_pieces();
            }
            int d = c - i;
            if(d > max_length)
            {
                max_length = d;
                longest_straight_line = i;
            }
        }
        std::cout << "The longest straight line starts from " << longest_straight_line << std::endl;
    }

    return longest_straight_line;
}
