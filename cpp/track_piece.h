#ifndef TRACK_PIECE_H
#define TRACK_PIECE_H

#include <iostream>

class track_piece
{
public:
    track_piece();
    track_piece(float length_, float radius_, float angle_, int lane_switch_, float dist);
    bool isBend();
    float getLength();
    float getRadius();
    float getAngle();
    void setMaxSpeed(float max_speed_) {max_speed = max_speed_;}
    float getMaxSpeed() {return max_speed;}
    int getLaneSwitch() {return lane_switch;}

private:
    float length;
    float radius;
    float angle;
    int lane_switch;
    float dist_from_start;
    float max_speed;
};

#endif // TRACK_PIECE_H
