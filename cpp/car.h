#ifndef CAR_H
#define CAR_H

#include <iostream>
#include "track.h"

class car
{
public:
    car();
    car(std::string name, std::string color, float length, float width, float guide_flag_pos);
    void setPosition(unsigned int idx, float pos);
    void setAngle(float angle_);
    void setTrack(track* racetrack_);
    void setCars(std::map<std::string,car>* cars_) { cars = cars_; }
    void setLane(unsigned int lane_);
    void setTurbo(float durationMilliseconds_, unsigned int durationTicks_, float factor_);
    float computeThrottle();
    float estimateAcceleration(float throttle, float cur_speed);
    unsigned int getPositionIndex();
    float getPiecePosition();
    float getSpeed();
    float getAngle();
    float getAngleSpeed();
    float getAngleAcc();
    int getLaneChange();
    float getTurboDurationMilliseconds();
    unsigned int getTurboDurationTicks();
    float getTurboFactor();
    void addToTurboDurationTicks(int ticks) { if(turboDurationTicks != 0 ) { turboDurationTicks += ticks; } }
    unsigned int getLane() { return lane; }
    std::string getColor() { return color; }

private:
    std::string name;
    std::string color;
    float length;
    float width;
    float guide_flag_pos;
    unsigned int lane;
    unsigned int piece_index;
    float in_piece_pos;
    float speed;
    float acc;
    float angle;
    float angle_speed;
    float angle_acc;
    track* racetrack;
    std::map<std::string,car>* cars;
    unsigned int estTickCount;
    int brake_until_piece;
    bool brake_until_piece_overflow;
    float brake_until_pos;
    float latest_ang_vel;
    float latest_ang_acc;
    float latest_ang;
    int lane_change;

    /* TURBO */
    float turboDurationMilliseconds;
    unsigned int turboDurationTicks;
    float turboFactor;
};

#endif // TRACK_PIECE_H
