#include "connection_text.h"
#include <unistd.h>

hwo_connection_text::hwo_connection_text() : hwo_connection()
{

}

hwo_connection_text::hwo_connection_text(const std::string& host, const std::string& port) : hwo_connection()
{

	JSONFile.open("json_data.txt");
}

hwo_connection_text::~hwo_connection_text()
{
	JSONFile.close();
}

jsoncons::json hwo_connection_text::receive_response(boost::system::error_code& error)
{
  usleep(16000);
  std::string str;
  if(!JSONFile.eof()) {
	  std::getline(JSONFile,str);
	  return jsoncons::json::parse_string(str);
  } else {
	  error = boost::asio::error::eof;
	  return jsoncons::json();
  }
}

void hwo_connection_text::send_requests(const std::vector<jsoncons::json>& msgs)
{
  // Do nothing
}
