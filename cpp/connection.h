#ifndef HWO_CONNECTION_H
#define HWO_CONNECTION_H

#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <jsoncons/json.hpp>
#include <fstream>

using boost::asio::ip::tcp;

class hwo_connection
{
public:
  hwo_connection();
  hwo_connection(const std::string& host, const std::string& port);
  virtual ~hwo_connection();
  virtual jsoncons::json receive_response(boost::system::error_code& error);
  virtual void send_requests(const std::vector<jsoncons::json>& msgs);

private:
  boost::asio::io_service io_service;
  tcp::socket socket;
  boost::asio::streambuf response_buf;
  std::ofstream JSONFile;	// File to write server json messages
};

#endif
